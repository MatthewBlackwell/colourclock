# Colour-Clock

https://mathiasblackwell.gitlab.io/colourclock/
(Direct Link to the Clock)

This is my interpretation of a clock, that isn't entirely a clock!

Using primarily CSS and JS, each div represents a different aspect of a clock.
The smallest bar, being a second, while the biggest being a month.

Depending if you view the clock at 9am on the 2nd of a month, or 9pm on the 22nd, you will get a different visual representation.

The DIVs overlay each other using blending modes in CSS, while the JS does all the mathemathics and timings of the clock. The two scripts working together in tandum. You will get different visualisations, depending on the day, hour, minute, and second, making it a unique look at every single time. 


24th of October but different times

00:10

https://media.discordapp.net/attachments/734852900689412288/1034038698628612157/unknown.png?width=2501&height=1221

10:36

https://media.discordapp.net/attachments/734852900689412288/1034037950473834526/unknown.png?width=2504&height=1221

23:39

https://media.discordapp.net/attachments/734852900689412288/1034038609533210624/unknown.png?width=2460&height=1221



