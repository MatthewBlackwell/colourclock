
//=========================================================================================variables
clock: new Date().getMilliseconds();

const months = document.getElementById("a30days");
const days = document.getElementById("a24hours");
const hours = document.getElementById("a60minutes");
const minutes = document.getElementById("a60seconds");
const seconds = document.getElementById("a1second");

let visible = "0";



//=========================================================================================functions

//update the whole clock
function update()
{
    time();
}


//time function
function time()
{

    let date = new Date();

    let monthDay = date.getDate();
    let monthDayMath = (90/30)*monthDay;
    let monthDaySet = monthDayMath + 'vw';
    months.style.width=monthDaySet;
    //document.getElementById("a30days").innerHTML= monthDay + " Day of the Month";

    //this is one day
    let day = date.getHours();
    let dayMath = (90/24)*day;
    let daySet = dayMath + 'vw';
    days.style.width= daySet;
    //document.getElementById("a24hours").innerHTML= day + " Hour of the Day";

    //this is one hour
    let hour = date.getMinutes();
    let hourMath = (90/60)*hour;
    let hourSet = hourMath + 'vw';
    hours.style.width= hourSet;
    //document.getElementById("a60minutes").innerHTML= hour + " Minute of the Hour";

    //this is one minute
    let min = date.getSeconds();
    let minuteMath = (90/60)*min;
    let minuteSet = minuteMath + 'vw';
    minutes.style.width= minuteSet;
    //document.getElementById("a60seconds").innerHTML= min + " Second of the Minute";

    //this is one second
    let sec = date.getMilliseconds();
    let secondMath = (90/1000)*sec;
    let secondSet = secondMath + 'vw';
    seconds.style.width= secondSet;

    if(day < 10)
    {
        day = "0"+day;
    }
    if(hour < 10)
    {
        hour = "0"+hour;
    }
    if(min < 10)
    {
        min = "0"+ min;
    }

    document.getElementById("title").innerHTML= day + ":"  + hour + ":" + min;

}



//================================================start the timeloop
window.requestAnimationFrame(gameLoop);
function gameLoop()
{
    update(); 
    requestAnimationFrame(gameLoop);  
}
